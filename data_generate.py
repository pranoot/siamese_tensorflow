import os, glob, cv2
import numpy as np
import tensorflow as tf


def read_image(src):
    DIM = 128
    im = cv2.imread(src, cv2.IMREAD_COLOR)
    im = cv2.resize(im, (DIM, DIM), interpolation=cv2.INTER_CUBIC)
    return im

# tfrecord scripts
path_tfrecords_test = 'test.tfrecords'

def wrap_int64(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def wrap_bytes(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def convert(image_paths, out_path):
    
    print("Converting: " + out_path)
    
    # Number of images. Used when printing the progress.
    num_images = len(image_paths)
    
    # # Open a TFRecordWriter for the output-file.
    with tf.python_io.TFRecordWriter(out_path) as writer:
        
        # Iterate over all the image-paths and class-labels.
        for i, (path) in enumerate(image_paths):

            # Load the image-file using matplotlib's imread function.
            img_1 = read_image(path[0])
            img_2 = read_image(path[1])
            label = int(path[2])

            
            # Convert the image to raw bytes.
            img_1_bytes = img_1.tostring()
            img_2_bytes = img_2.tostring()

            # Create a dict with the data we want to save in the
            # TFRecords file. You can add more relevant data here.
            data = \
                {
                    'image_1': wrap_bytes(img_1_bytes),
                    'image_2': wrap_bytes(img_2_bytes),
                    'label': wrap_int64(label)
                }

            # Wrap the data as TensorFlow Features.
            feature = tf.train.Features(feature=data)

            # Wrap again as a TensorFlow Example.
            example = tf.train.Example(features=feature)

            # Serialize the data.
            serialized = example.SerializeToString()
            
            # Write the serialized data to the TFRecords file.
            writer.write(serialized)



if '__name__' == '__main__':

    test_dirname = '/home/pranoot/Desktop/DFW-dataset/DFW_Data/Testing_data_frames/'
    train_dirname = '/home/pranoot/Desktop/DFW-dataset/DFW_Data/Training_data_frames/'

    test_folders = glob.glob(test_dirname+'*')


    data = []
    for test_folder in test_folders:
        
        files = glob.glob(test_folder+'/*')
        personality_name = test_folder.split('/')[-1]
        orig_tmp = [filename for filename in files if '_h_0' not in filename and '_I_0' not in filename and '_a.jpg' not in filename]
        val_tmp = [filename for filename in files if '_a.jp' in filename]
        dis_tmp = [filename for filename in files if '_h_0'in filename]
        imp_tmp = [filename for filename in files if '_I_0'in filename]
        
        for dis_file in dis_tmp:
            pair = (orig_tmp[0], dis_file, 1)
            data.append(pair)
        
        for imp_file in imp_tmp:
            pair = (orig_tmp[0], imp_file, 0)
            data.append(pair)


    np.save('data.npy', data)
    data = np.load('data.npy')


    convert(image_paths=data, out_path=path_tfrecords_test)
    