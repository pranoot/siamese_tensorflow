class Constant():

    channels = 3
    height = 64
    width = 64
    input_shape = [None, height, width, channels]
    batch_size = 16
