import tensorflow as tf
from constants import Constant

class siamese:

    # Create model
    def __init__(self):
        self.x1 = tf.placeholder(tf.float32, Constant.input_shape, name='x1')
        self.x2 = tf.placeholder(tf.float32, Constant.input_shape, name='x2')

        with tf.variable_scope("siamese") as scope:
            self.o1 = self.network(self.x1)
            scope.reuse_variables()
            self.o2 = self.network(self.x2)

        # Create loss
        self.y_ = tf.placeholder(tf.float32, [None])
        self.loss = self.loss_with_spring()
        # self.triplet_loss = self.triplet_loss()


    def network_old(self, x):
        weights = []
        fc1 = self.fc_layer(x, 1024, "fc1")
        ac1 = tf.nn.relu(fc1)
        fc2 = self.fc_layer(ac1, 1024, "fc2")
        ac2 = tf.nn.relu(fc2)
        fc3 = self.fc_layer(ac2, 2, "fc3")
        return fc3


    def network(self, x):
        weights = []
        
        conv1 = self.conv2d_layer(x, [3,3], 8, 'conv1')
        a_conv1 = tf.nn.relu(conv1)

        conv2d = self.conv2d_layer(a_conv1, [3,3], 16, 'conv2')
        a_conv2 = tf.nn.relu(conv2)

        conv3 = self.conv2d_layer(a_conv3, [3,3], 32, 'conv3')
        a_conv3 = tf.nn.relu(conv3)

        flatten_input = tf.contrib.layers.flatten(a_conv3)

        fc1 = self.fc_layer(flatten_input, 1024, "fc1")
        ac1 = tf.nn.relu(fc1)
        ac1 = tf.contrib.layers.dropout(ac1, keep_prob=0.5)

        fc2 = self.fc_layer(ac1, 1024, "fc2")
        ac2 = tf.nn.relu(fc2)
        ac2 = tf.contrib.layers.dropout(ac2, keep_prob=0.5)

        fc3 = self.fc_layer(ac2, 2, "fc3")
        return fc3


    def fc_layer(self, bottom, n_weight, name):
        assert len(bottom.get_shape()) == 2
        n_prev_weight = bottom.get_shape()[1]
        initer = tf.truncated_normal_initializer(stddev=0.01)
        W = tf.get_variable(name+'W', dtype=tf.float32, shape=[n_prev_weight, n_weight], initializer=initer)
        b = tf.get_variable(name+'b', dtype=tf.float32, initializer=tf.constant(0.01, shape=[n_weight], dtype=tf.float32))
        fc = tf.nn.bias_add(tf.matmul(bottom, W), b)
        return fc


    def conv2d_layer(self, bottom, kernal_shape, filters, name, padding = 'VALID'):
        assert len(bottom.get_shape()) == 2
        n_prev_weight = bottom.get_shape()[1]
        initer = tf.contrib.layers.xavier_initializer_conv2d()
        W = tf.get_variable(name+'W', dtype=tf.float32, shape=[kernal_shape[0], kernal_shape[1], n_prev_weight, filters], initializer=initer)
        b = tf.get_variable(name+'b', dtype=tf.float32, initializer=tf.constant(0.01, shape=[filters], dtype=tf.float32))
        z = tf.nn.conv2d(bottom, W, strides=[1, 1, 1, 1], padding=padding)
        conv2d = tf.nn.bias_add(z, b)
        return conv2d


    def loss_with_spring(self):
        margin = 5.0
        labels_t = self.y_
        labels_f = tf.subtract(1.0, self.y_, name="1-yi")          # labels_ = !labels;
        eucd2 = tf.pow(tf.subtract(self.o1, self.o2), 2)
        eucd2 = tf.reduce_sum(eucd2, 1)
        eucd = tf.sqrt(eucd2+1e-6, name="eucd")
        # C = tf.constant(margin, name="C")
        # yi*||CNN(p1i)-CNN(p2i)||^2 + (1-yi)*max(0, C-||CNN(p1i)-CNN(p2i)||^2)
        pos = tf.multiply(labels_t, eucd2, name="yi_x_eucd2")
        # neg = tf.multiply(labels_f, tf.subtract(0.0,eucd2), name="yi_x_eucd2")
        # neg = tf.multiply(labels_f, tf.maximum(0.0, tf.subtract(C,eucd2)), name="Nyi_x_C-eucd_xx_2")
        neg = tf.multiply(labels_f, tf.pow(tf.maximum(tf.subtract(C, eucd), 0), 2), name="Nyi_x_C-eucd_xx_2")
        losses = tf.add(pos, neg, name="losses")
        loss = tf.reduce_mean(losses, name="loss")
        return loss


    def loss_with_step(self):

        margin = 5.0
        labels_t = self.y_
        labels_f = tf.subtract(1.0, self.y_, name="1-yi")          # labels_ = !labels;
        eucd2 = tf.pow(tf.subtract(self.o1, self.o2), 2)
        eucd2 = tf.reduce_sum(eucd2, 1)
        eucd = tf.sqrt(eucd2+1e-6, name="eucd")
        C = tf.constant(margin, name="C")
        pos = tf.multiply(labels_t, eucd, name="y_x_eucd")
        neg = tf.multiply(labels_f, tf.maximum(0.0, tf.subtract(C, eucd)), name="Ny_C-eucd")
        losses = tf.add(pos, neg, name="losses")
        loss = tf.reduce_mean(losses, name="loss")
        return loss


    # def triplet_loss()


    def contrastive_loss(self, y, d):
        tmp= y*tf.square(d)
        #tmp= tf.mul(y,tf.square(d))
        tmp2 = (1-y) *tf.square(tf.maximum((1 - d),0))
        return tf.reduce_sum(tmp +tmp2)/Constant.batch_size/2

